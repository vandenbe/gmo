# Gradient-based marginal optimization

Works nicely with latent-Gaussian models having log-concave densities (such as the log-Gaussian Cox process).

## Citation

This is a fork of https://github.com/stan-dev/gmo

See also

+ Dustin Tran, Andrew Gelman, and Aki Vehtari. 2016. Gradient-based marginal optimization. In preparation.

```
@article{tran2016gmo,
  title = {Gradient-based marginal optimization},
  author = {Dustin Tran and Andrew Gelman and Aki Vehtari},
  journal = {In preparation},
  year = {2016}
}
```
